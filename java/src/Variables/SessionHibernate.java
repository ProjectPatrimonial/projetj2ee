package Variables;
  
import org.hibernate.Session;  
import org.hibernate.SessionFactory; 
import org.hibernate.boot.Metadata;  
import org.hibernate.boot.MetadataSources;  
import org.hibernate.boot.registry.StandardServiceRegistry;  
import org.hibernate.boot.registry.StandardServiceRegistryBuilder; 

public class SessionHibernate {
    
    private static final SessionHibernate instance = new SessionHibernate();
    private Session session;
    private SessionFactory factory;
    //private constructor to avoid client applications to use constructor
    private SessionHibernate(){
    	StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
        Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  
         
       factory = meta.getSessionFactoryBuilder().build();  
    }

    public static SessionHibernate getInstance(){
        return instance;
    }
    public Session getSession(){
        return session=factory.openSession();
    }
    public void close() {
        session.close(); 
    }
}