package traiter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import machines.CoordonneesGPS;
import machines.Distributeur;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
/**
 * Servlet implementation class update
 */
@WebServlet("/update")
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Session session= Variables.SessionHibernate.getInstance().getSession();
		Transaction t = session.beginTransaction();
		Query query = session.createQuery("From Distributeur WHERE numSerie ="+request.getParameter("numSerie"));
		Distributeur temp = (Distributeur) query.list().get(0);
		Distributeur dist = (Distributeur) session.load(machines.Distributeur.class,temp.getDistrib_id());
		dist.setNumSerie((String)request.getParameter("numSerie"));
		dist.setAdresse((String)request.getParameter("adresse"));
		dist.setLocalisation((String)request.getParameter("localisation"));
		double Longitude=(Double.parseDouble(request.getParameter("longitude")));
		double Lattitude=(Double.parseDouble(request.getParameter("latitude")));
		if(Longitude!=0 && Lattitude!=0)
			dist.setCoord(new CoordonneesGPS(Lattitude,Longitude));
		dist.setCategorie((String)request.getParameter("Categorie"));
		try {
			DateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
			dist.setDernIntervention(simpleDateFormat.parse(request.getParameter("dernIntervention")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dist.setCommentaires((String)request.getParameter("commentaires"));
		session.save(dist);
		t.commit();
		Variables.SessionHibernate.getInstance().close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
