package traiter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import machines.CoordonneesGPS;
import machines.Distributeur;
import machines.Produit;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
/**
 * Servlet implementation class update
 */
@WebServlet("/ajout")
public class ajout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ajout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String numSerie=(String)request.getParameter("numSerie");
		String adresse =(String)request.getParameter("adresse");
		String localisation=(String)request.getParameter("localisation");
		double Longitude=(Double.parseDouble(request.getParameter("longitude")));
		double Lattitude=(Double.parseDouble(request.getParameter("latitude")));
		String Categorie=(String)request.getParameter("Categorie");
		String commentaires =(String)request.getParameter("commentaires");
		System.out.println(numSerie);
		System.out.println(adresse+localisation+Lattitude+Longitude+Categorie+commentaires);
		Distributeur dist=new Distributeur(numSerie,adresse,localisation,Lattitude,Longitude,Categorie,commentaires);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
