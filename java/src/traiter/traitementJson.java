package traiter;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONObject;

import machines.Datetab;
import machines.Distributeur;
import machines.Integertab;
import machines.MoyenPaiement;
import machines.Produit;
import machines.Rapport;
import machines.Stock;
import machines.Stringtab;



/**
 * Servlet implementation class traitement
 */
@WebServlet("/traitementJson")
public class traitementJson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public traitementJson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		BufferedReader read = request.getReader();
		String c="";
		String c2;
		while((c2=read.readLine())!=null) {
			c+=c2+"\n";
		}
		if(c!="") {
			try {	
				 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			     JSONObject jsonObject = new JSONObject(c);
			     int champ =jsonObject.getJSONObject("automate").getInt("idAutomate");
			     Session session= Variables.SessionHibernate.getInstance().getSession();
				 Distributeur distrib= session.get(Distributeur.class, champ);
				 Query query = session.createQuery("From Rapport WHERE distrib_id ="+champ);
				 List<Rapport> lrap=query.list();
				 Variables.SessionHibernate.getInstance().close();
			     String etat =jsonObject.getJSONObject("automate").getString("etat");
			     List<Datetab> historiques = new ArrayList<Datetab>();
				 List<Stringtab> raisons = new ArrayList<Stringtab>();
				 String history="";
				 for(int i=0;i<jsonObject.getJSONObject("automate").getJSONArray("historique").length();i++) {
					 historiques.add(new Datetab(simpleDateFormat.parse(jsonObject.getJSONObject("automate").getJSONArray("historique").getJSONObject(i).getString("date"))));
				     raisons.add(new Stringtab(jsonObject.getJSONObject("automate").getJSONArray("historique").getJSONObject(i).getString("erreur")));
				     history+="\n Date : " +jsonObject.getJSONObject("automate").getJSONArray("historique").getJSONObject(i).getString("date") + "     Erreur : " + jsonObject.getJSONObject("automate").getJSONArray("historique").getJSONObject(i).getString("erreur");
				 }
				 List<Produit> produits = new ArrayList<Produit>();
				 List<Integertab> quantities = new ArrayList<Integertab>();
				 String enstock="";
				 for(int i=0;i<jsonObject.getJSONObject("automate").getJSONArray("Stock").length();i++) {
					 produits.add(new Produit(jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getString("produit"),jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getFloat("prix")));
				     quantities.add(new Integertab(jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getInt("quantite")));
				     enstock+="\n Produit : "+jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getString("produit")+"      Prix : " +jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getFloat("prix")+ "      Quantite : "+ jsonObject.getJSONObject("automate").getJSONArray("Stock").getJSONObject(i).getInt("quantite");
				 }
				 String categorie=jsonObject.getJSONObject("automate").getString("Categorie");
				 Stock stock= new Stock(categorie,produits,quantities);
				 String moyenspaiement="";
			     Date date =simpleDateFormat.parse(jsonObject.getJSONObject("automate").getString("date"));
			     List<MoyenPaiement> paiements = new ArrayList<MoyenPaiement>();
			     for(int i=0;i<jsonObject.getJSONObject("automate").getJSONArray("paiements").length();i++) {
				     paiements.add(new MoyenPaiement(jsonObject.getJSONObject("automate").getJSONArray("paiements").getJSONObject(i).getString("type"),jsonObject.getJSONObject("automate").getJSONArray("paiements").getJSONObject(i).getString("statut")));
				     moyenspaiement+= "\n Type paiement : " +jsonObject.getJSONObject("automate").getJSONArray("paiements").getJSONObject(i).getString("type")+"      Etat :  " + jsonObject.getJSONObject("automate").getJSONArray("paiements").getJSONObject(i).getString("statut");
			     }
			     double temperature = Float.parseFloat(traiter.traitementTemperature.getTemperature(distrib.getAdresse()).toString());
			     double montant =jsonObject.getJSONObject("automate").getFloat("montant");
			     String statut=jsonObject.getJSONObject("automate").getString("statut");
			     distrib.setRapports(lrap);
			     System.out.println(distrib.getRapports().size());
			     distrib.addRapport(etat, stock, date, paiements, temperature, montant, statut,historiques,raisons);
			     System.out.println("\n Recu rapport JSON:\n Machine num "+champ+" dans l'�tat "+etat+" en stock on a "+enstock+" on est le "+date+" tu peux payer par "+moyenspaiement+" il fait "+temperature+" degr�s on s'est fait "+montant+" euros et sinon on est "+statut+"\n historique des events :"+history);
			}catch (Exception err){
				err.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}