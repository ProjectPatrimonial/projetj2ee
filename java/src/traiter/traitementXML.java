package traiter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import machines.Datetab;
import machines.Distributeur;
import machines.Integertab;
import machines.MoyenPaiement;
import machines.Produit;
import machines.Rapport;
import machines.Stock;
import machines.Stringtab;


/**
 * Servlet implementation class traitement
 */
@WebServlet("/traitementXML")
public class traitementXML extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public traitementXML() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		BufferedReader read = request.getReader();
		String c="";
		String c2;
		while((c2=read.readLine())!=null) {
			c+=c2+"\n";
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;
		if(c!="") {
			try {  
				
			    builder = factory.newDocumentBuilder();  
			    
			    Document document = builder.parse(new InputSource(new StringReader(c)));  
			    String champ=document.getElementsByTagName("idAutomate").item(0).getTextContent();
			    Session session= Variables.SessionHibernate.getInstance().getSession();
			    Distributeur distrib= session.get(Distributeur.class, Integer.parseInt(champ));
			    Query query = session.createQuery("From Rapport WHERE distrib_id ="+champ);
			    List<Rapport> lrap=query.list();
			    Variables.SessionHibernate.getInstance().close();
			    
			    String etat=document.getElementsByTagName("etat").item(0).getTextContent();
	  
			    String enstock="";
			    NodeList stocks = document.getElementsByTagName("produit");
			    List<Produit> produits = new ArrayList<Produit>();
			    List<Integertab> quantities = new ArrayList<Integertab>();
			    for(int i =0;i<stocks.getLength();i++) {
			    	 quantities.add(new Integertab(Integer.parseInt(((Element)stocks.item(i)).getElementsByTagName("quantite").item(0).getTextContent())));
			    	 produits.add(new Produit(((Element)stocks.item(i)).getElementsByTagName("nom").item(0).getTextContent(),Integer.parseInt(((Element)stocks.item(i)).getElementsByTagName("prix").item(0).getTextContent())));
			    	 enstock+=((Element)stocks.item(i)).getElementsByTagName("nom").item(0).getTextContent()+ " : ";
			    	 enstock+=Integer.parseInt(((Element)stocks.item(i)).getElementsByTagName("quantite").item(0).getTextContent())+"\n";
			    }
			    
			    String categorie=document.getElementsByTagName("Categorie").item(0).getTextContent();
			    
			    Stock stock= new Stock(categorie, produits, quantities);
			 
			    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			    Date date = simpleDateFormat.parse(document.getElementsByTagName("Date").item(0).getTextContent());

			    double montant=Float.parseFloat(document.getElementsByTagName("montant").item(0).getTextContent());
			    String moyenspaiement="";
			    List<MoyenPaiement> paiements = new ArrayList<MoyenPaiement>();
			    NodeList nodepaiement=document.getElementsByTagName("MoyPay");
			    for(int i=0; i<nodepaiement.getLength();i++) {
			    	paiements.add(new MoyenPaiement(((Element)nodepaiement.item(i)).getElementsByTagName("type").item(0).getTextContent(),((Element)nodepaiement.item(i)).getElementsByTagName("etat").item(0).getTextContent()));
			    	moyenspaiement+=((Element)nodepaiement.item(i)).getElementsByTagName("type").item(0).getTextContent()+" : "+((Element)nodepaiement.item(i)).getElementsByTagName("etat").item(0).getTextContent()+"\n";
			    }
			    
			    String history="";
			    List<Datetab> historiques = new ArrayList<Datetab>();
			    List<Stringtab> raisons = new ArrayList<Stringtab>();
			    NodeList histo=document.getElementsByTagName("event");
			    for(int i=0; i<histo.getLength();i++) {
			    	historiques.add(new Datetab(simpleDateFormat.parse(((Element)histo.item(i)).getElementsByTagName("date").item(0).getTextContent())));
			    	raisons.add(new Stringtab(((Element)histo.item(i)).getElementsByTagName("descr").item(0).getTextContent()));
			    	history+="\n"+simpleDateFormat.parse(((Element)histo.item(i)).getElementsByTagName("date").item(0).getTextContent());
			    	history+=": "+((Element)histo.item(i)).getElementsByTagName("descr").item(0).getTextContent();
			    }
			   
			    String statut=document.getElementsByTagName("statut").item(0).getTextContent();
		
			    double temperature = Float.parseFloat(traiter.traitementTemperature.getTemperature(distrib.getAdresse()).toString());
			    distrib.setRapports(lrap);
			     System.out.println(distrib.getRapports().size());
			    distrib.addRapport(etat, stock, date, paiements, temperature, montant, statut,historiques,raisons);
			    System.out.println("Recu rapport XML:\n Machine num "+champ+" dans l'�tat "+etat+" en stock on a "+enstock+" on est le "+date+" tu peux payer par "+moyenspaiement+" il fait "+temperature+" degr�s on s'est fait "+montant+" euros et sinon on est "+statut+"\n historique des events :"+history);
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}