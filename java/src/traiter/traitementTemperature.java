package traiter;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.JSONTokener;


public class traitementTemperature {


	/**
	 * @throws IOException 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected static Double getTemperature(String ville) throws IOException {
		// TODO Auto-generated method stub
		URL url = new URL("http://api.openweathermap.org/data/2.5/weather?units=metric&lang=fr&appid=0ec1f169aa0bd928c48011d06b3bca2b&q="+ville);
		URLConnection urlConn = url.openConnection();
		JSONObject json = new JSONObject(new JSONTokener(urlConn.getInputStream()));
		return json.getJSONObject("main").getDouble("temp");

	}



}
