package machines;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class StoreData {
	public static void main( String[] args ) throws Exception  
    {  
		
       
       Produit p1= new Produit("BN", 1.80);
       Produit p2= new Produit("Lays", 1.50);
       Produit p3= new Produit("Oasis", 1.00);

       List<Produit> produits = new ArrayList<Produit>();
       produits.add(p1);
       produits.add(p2);
       produits.add(p3);
       System.out.println("ah");
       List<Integertab> quantities = new ArrayList<Integertab>();
       Integertab i1= new Integertab(10);
       Integertab i2= new Integertab(5);
       Integertab i3= new Integertab(3);
       quantities.add(i1);
       quantities.add(i2);
       quantities.add(i3);
       System.out.println("ah");
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
       List<Datetab> dateIncident = new ArrayList<Datetab>();
       dateIncident.add(new Datetab(simpleDateFormat.parse("02-01-1997")));
       dateIncident.add(new Datetab(simpleDateFormat.parse("12-03-1997")));
       Date dateIntervention= simpleDateFormat.parse("14-03-2000");
       List<Stringtab> raison = new ArrayList<Stringtab>();
       Stringtab s1=new Stringtab("panne pieces");
       Stringtab s2=new Stringtab("pu de papier");
       raison.add(s1);
       raison.add(s2);
       System.out.println("ah");
       List<MoyenPaiement> paiements = new ArrayList<MoyenPaiement>();
       paiements.add(new MoyenPaiement("piece","correct"));
       paiements.add(new MoyenPaiement("CB","panne"));
       CoordonneesGPS coord=new CoordonneesGPS(10.5,5.6);
       System.out.println("ah");
       
       Stock s = new Stock("Encas", produits, quantities); 
       
       
      
      ArrayList<Rapport> rapports = new ArrayList<Rapport>();
   
      Distributeur d = new Distributeur("1","Paris","localisation",1.1,1.1,"Encas","commentaire");
      d.addRapport("etat", s, dateIntervention, paiements, 17.5, 93.4, "statut", dateIncident, raison);
       
       
       System.out.println("ah");
       

    }  
}
