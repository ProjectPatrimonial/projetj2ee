package machines;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Stock {
	private int stock_id;
	private String Categorie;
	private List<Produit> produits;
	private List<Integertab> quantity;
	public Stock(String Categorie, List<Produit> produits, List<Integertab> quantities) {
		assert(produits.size()==quantities.size());
		this.Categorie = Categorie;
		this.produits = produits;
		this.quantity = quantities;
		this.AddBDDStock();
	}
	public Stock() {
	
	}
	public int getStock_id() {
		return stock_id;
	}
	public void setStock_id(int stock_id) {
		this.stock_id = stock_id;
	}
	public List<Produit> getProduits() {
		return produits;
	}
	public void setProduits(List<Produit> produitlist) {
		this.produits = produitlist;
	}
	public List<Integertab> getQuantity() {
		return quantity;
	}
	public void setQuantity(List<Integertab> quantity) {
		this.quantity = quantity;
	}
	public void setCategorie(String categorie) {
		Categorie = categorie;
	}
	public String getCategorie() {
		return Categorie;
	}
	public Integertab getQuantity(int indice) {
		return quantity.get(indice);
	}
	public void setQuantity(int indice, Integer q) {
		quantity.set(indice, new Integertab(q));
	}
	public int getSize() {
		return produits.size();
	}
	public void AddBDDStock() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
}
