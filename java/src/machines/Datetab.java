package machines;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Datetab {
	
	private Date laDate;
	private int id;
	
	public Datetab(Date laDate) {
		this.laDate=laDate;
		this.AddBDDDatetab();
	}

	public Date getLaDate() {
		return laDate;
	}

	public void setLaDate(Date laDate) {
		this.laDate = laDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public void AddBDDDatetab() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}

}
