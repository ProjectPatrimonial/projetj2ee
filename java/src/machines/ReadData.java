package machines;

import java.text.ParseException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class ReadData {
	 
	public static void afficherDistributeur(int n) {
		Session session= Variables.SessionHibernate.getInstance().getSession();
		Distributeur distrib= session.get(Distributeur.class, n);
        System.out.println("\n Num�ro de S�rie: " + distrib.getDistrib_id());
        System.out.println("\n Type: " + distrib.getCategorie());
        System.out.println("\n Adresse: " + distrib.getAdresse());
        System.out.println("\n Localisation: " + distrib.getLocalisation());
        System.out.println("\n Coordonnees: lat -> " + distrib.getCoord().getLatitude() + ", long -> "+ distrib.getCoord().getLongitude());
        System.out.println("\n Date de la derni�re intervention: " + distrib.getDernIntervention());
        Variables.SessionHibernate.getInstance().close();
	}
	public static void main( String[] args )   
    {  
		afficherDistributeur(1);
    }
}
