package machines;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Produit {
	private int produit_id;
	public int getProduit_id() {
		return produit_id;
	}
	public Produit() {}
	
	public void setProduit_id(int produit_id) {
		this.produit_id = produit_id;
	}
	private String nom;
	private double prix;
	public Produit(String nom, double d) {
		this.nom = nom;
		this.prix = d;
		this.AddBDDProduit();
	}
	public String getNom() {
		return nom;
	}
	public void AddBDDProduit() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}	
}
