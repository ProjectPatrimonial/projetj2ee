package machines;


import org.hibernate.Session;
import org.hibernate.Transaction;

public class UpdateData {
	public static void main( String[] args )   
    {  
		//modifie la valeur de la latitude pour la coordGPS d'id 3
		Session session= Variables.SessionHibernate.getInstance().getSession();
		Transaction t = session.beginTransaction();
		CoordonneesGPS coord = (CoordonneesGPS) session.load(machines.CoordonneesGPS.class,new Integer(3));
		coord.setLatitude(3.3);
		session.save(coord);
		t.commit();
		Variables.SessionHibernate.getInstance().close();
		
    }
}
