package machines;


import org.hibernate.Session;
import org.hibernate.Transaction;

public class CoordonneesGPS {
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	private double latitude;
	public int getCoord_id() {
		return coord_id;
	}
	public void setCoord_id(int coord_id) {
		this.coord_id = coord_id;
	}
	private double longitude;
	private int coord_id;
	public CoordonneesGPS(double latitude,double longitude){
		this.latitude=latitude;
		this.longitude=longitude;
		this.AddBDDCoordonneesGPS();
		
	}
	public CoordonneesGPS() {
	}
	
	public void AddBDDCoordonneesGPS() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
	public String toString() {
		return "( latitude: "+latitude+"; longitude: "+longitude+" )";
	}
}
