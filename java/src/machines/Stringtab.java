package machines;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Stringtab {
	private int string_id;
	private String unString;
	public Stringtab(String unString) {
		this.unString=unString;
		this.AddBDDStringtab();
	}
	public int getString_id() {
		return string_id;
	}
	public void setString_id(int string_id) {
		this.string_id = string_id;
	}
	public String getUnString() {
		return unString;
	}
	public void setUnString(String unString) {
		this.unString = unString;
	}
	public void AddBDDStringtab() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}

}
