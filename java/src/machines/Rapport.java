package machines;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Date;

public class Rapport {
	private int rapport_id;
	private String etat;// ok ou attention ou erreur ou alarme
	private List<Datetab> dateIncident;
	private List<Stringtab> raison;
	private Stock s;
	private Date dateIntervention;
	private List<MoyenPaiement> paiements;
	private double montant;
	private String statut;//en service ou HS
	public Rapport(String etat, Stock s, Date dateIntervention, List<MoyenPaiement> paiements,
			double montant, String statut, List<Datetab> dateIncident, List<Stringtab> raison) {
		this.etat = etat;
		this.s = s;
		this.dateIntervention = dateIntervention;
		this.paiements = paiements;
		this.montant = montant;
		this.statut=statut;
		assert dateIncident.size()==raison.size();
		this.dateIncident=dateIncident;
		this.raison=raison;
	}
	public Rapport() {
	
	}
	public int getRapport_id() {
		return rapport_id;
	}
	public void setRapport_id(int rapport_id) {
		this.rapport_id = rapport_id;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public List<Datetab> getDateIncident() {
		return dateIncident;
	}
	public void setDateIncident(List<Datetab> dateIncident) {
		this.dateIncident = dateIncident;
	}
	public List<Stringtab> getRaison() {
		return raison;
	}
	public void setRaison(List<Stringtab> raison) {
		this.raison = raison;
	}
	public Stock getS() {
		return s;
	}
	public void setS(Stock s) {
		this.s = s;
	}
	public Date getDateIntervention() {
		return dateIntervention;
	}
	public void setDateIntervention(Date dateIntervention) {
		this.dateIntervention = dateIntervention;
	}
	public List<MoyenPaiement> getPaiements() {
		return paiements;
	}
	public void setPaiements(List<MoyenPaiement> paiements) {
		this.paiements = paiements;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public void AddBDDRapport() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
}
