package machines;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class MoyenPaiement {
	private String nom;
	private String etat;
	private int moyenPaiement_id;
	public int getMoyenPaiement_id() {
		return moyenPaiement_id;
	}
	public void setMoyenPaiement_id(int moyenPaiement_id) {
		this.moyenPaiement_id = moyenPaiement_id;
	}
	public MoyenPaiement(String nom, String etat) {
		this.nom=nom;
		this.etat=etat;
		this.AddBDDMoyenPaiement();
	}
	public String getNom() {
		return nom;
	}
	public String getEtat() {
		return etat;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public void AddBDDMoyenPaiement() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
	
}
