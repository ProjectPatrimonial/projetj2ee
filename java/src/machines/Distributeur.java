package machines;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Date;

public class Distributeur {
	private static int idGenerator=0;
	private int distrib_id;
	private String numSerie;
	private int coord_id;
	private String adresse;//ville
	private String localisation; //emplacement
	private CoordonneesGPS coord;
	private double montantGagne;
	private String Categorie; //type
	private List<Rapport> rapports;
	private Date dernIntervention;
	private String commentaires;
	private int requis;
	private double temperature;
	public Distributeur(String numSerie, String adresse, String localisation, double latitude, double longitude,
			String Categorie, String commentaires) {
		this.distrib_id = ++idGenerator;
		this.numSerie = numSerie;
		this.adresse = adresse;
		this.localisation = localisation;
		this.coord = new CoordonneesGPS(latitude,longitude);
		this.montantGagne = 0;
		this.rapports = new ArrayList<Rapport>();
		this.Categorie=Categorie;
		this.dernIntervention=new Date();
		this.commentaires=commentaires;
		this.requis=10;
		this.temperature=10;
		this.AddBDDDistributeur();
	}
	
	
	public Distributeur() {
		
	}
	public static int getIdGenerator() {
		return idGenerator;
	}
	public static void setIdGenerator(int idGenerator) {
		Distributeur.idGenerator = idGenerator;
	}
	public int getDistrib_id() {
		return distrib_id;
	}
	public void setDistrib_id(int distrib_id) {
		this.distrib_id = distrib_id;
	}
	public String getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	public String getLocalisation() {
		return localisation;
	}
	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
	public CoordonneesGPS getCoord() {
		return coord;
	}
	public void setCoord(CoordonneesGPS coord) {
		this.coord = coord;
	}
	public double getMontantGagne() {
		return montantGagne;
	}
	public void setMontantGagne(double montantGagne) {
		this.montantGagne = montantGagne;
	}
	public String getCategorie() {
		return Categorie;
	}
	public void setCategorie(String categorie) {
		Categorie = categorie;
	}
	public List<Rapport> getRapports() {
		return rapports;
	}
	public void setRapports(List<Rapport> rapports) {
		this.rapports = rapports;
	}
	public Date getDernIntervention() {
		return dernIntervention;
	}
	public void setDernIntervention(Date dernIntervention) {
		this.dernIntervention = dernIntervention;
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public void addRapport(String etat, Stock S, Date dateIntervention, List<MoyenPaiement> paiements, double temperature,
			double montant,String statut,List<Datetab> dateIncident, List<Stringtab> raison) throws Exception {
		if(!S.getCategorie().equals(Categorie)) throw new Exception("not good type of products");
		Rapport r = new Rapport(etat, S, dateIntervention, paiements, montant, statut,dateIncident,raison);
		System.out.println(etat+"    "+dateIntervention+"     "+montant+"     "+statut+"  "+dateIncident+"   "+paiements.size()+" "+S.getStock_id()+"   "+raison.size());
		this.dernIntervention=dateIntervention;
		this.rapports.add(r);
		r.AddBDDRapport();
		this.montantGagne+=montant;
		if(this.temperature<5 && temperature>=5 && this.Categorie=="boissons chaudes") changerRequis(-3);//normaux
		else if(this.temperature>=5 && temperature<5 && this.Categorie=="boissons chaudes") changerRequis(3);//*3
		if(this.temperature<25 && temperature>=25 && this.Categorie=="boissons froides") changerRequis(3);//*3
		else if(this.temperature>=25 && temperature<25 && this.Categorie=="boissons froides") changerRequis(-3);//normaux
		this.temperature=temperature;
		
		
	}


	private void changerRequis(int mult) {
		this.requis=mult;
	}
	public int getRequis() {
		return requis;
	}


	public void setRequis(int requis) {
		this.requis = requis;
	}


	public String getAdresse() {
		return adresse;
	}
	public void AddBDDDistributeur() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}
	public int getCoord_id() {
		return coord_id;
	}
	public void setCoord_id(int coord_id) {
		this.coord_id = coord_id;
	}
}
