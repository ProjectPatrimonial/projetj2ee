package machines;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Integertab {

	private int n;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integertab(int n) {
		this.n=n;
		this.AddBDDIntegertab();
	}
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public void AddBDDIntegertab() {
		Session session= Variables.SessionHibernate.getInstance().getSession();
        Transaction t = session.beginTransaction();  
        session.save(this);  
        t.commit(); 
        Variables.SessionHibernate.getInstance().close();
	}

}
